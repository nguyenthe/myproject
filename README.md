This is My Code
Project include some components like as :

    + data
        - networking (service to connect to link api and get data from api).
        
    + ui
        
        - base folder (include base code for BaseActivity, BasePresenter,interface MVPView, MVPPresenter and interface for some function general )
        - dialog(include DialogLoading, need to info notification about status internet).
        - main(include Activity, adapter ).
        
    + utils
        - injects folder : getinstance API Implement class then inject to presenter.
        - Class include somes functions to logic : randomColor, check status connection.
    
    + android test :
        - DataInteractorTest : test get data from api
        - MainPresenterTest : test inject from presenter.
        