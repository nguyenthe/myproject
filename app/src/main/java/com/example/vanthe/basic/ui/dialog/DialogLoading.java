package com.example.vanthe.basic.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.LinearLayout;

import com.example.vanthe.basic.R;

public class DialogLoading extends Dialog {

    public DialogLoading(Context context) {
        super(context, R.style.AppDialogTheme);
        // TODO Auto-generated constructor stub
        Window winD = getWindow();
        if (winD != null) {
            winD.getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_layout);
        getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
    }
}

