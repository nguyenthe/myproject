package com.example.vanthe.basic.ui.main.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.vanthe.basic.R;
import com.example.vanthe.basic.utils.UIUtils;

public class HolderHomeAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {

    private HomeAdapter.ClickListener mClickListener;
    private TextView mTvContent;
    private CardView mCardView;

    public HolderHomeAdapter(View itemView) {
        super(itemView);
        mTvContent = itemView.findViewById(R.id.tvContent);
        mCardView = itemView.findViewById(R.id.mCardView);
        itemView.setOnClickListener(this);
    }

    public void bind(String content, HomeAdapter.ClickListener clickListener) {
        this.mClickListener = clickListener;
        mTvContent.setText(UIUtils.getFormatString(content));
        Log.e(HolderHomeAdapter.class.getName(), " string two line " + UIUtils.getFormatString(content));
        mCardView.setCardBackgroundColor(UIUtils.getRandomColor());
    }

    @Override
    public void onClick(View view) {
        if (mClickListener != null) {
            mClickListener.onItemClicked(view, getAdapterPosition());
        }
    }
}
