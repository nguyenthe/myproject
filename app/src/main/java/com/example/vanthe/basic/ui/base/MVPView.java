package com.example.vanthe.basic.ui.base;

import android.support.annotation.StringRes;

public interface MVPView {

    void showLoading();

    void hideLoading();

    void onError(String message);

    void onErrorDialog(String message);

    void onError(@StringRes int resId);

    void onConnectToServerError();

    void onNoInternetConnection();

    boolean isNetworkConnected();

    void hideKeyboard();

}
