package com.example.vanthe.basic.ui.main;

import android.os.Handler;
import android.util.Log;

import com.example.vanthe.basic.data.networking.ApiHelper;
import com.example.vanthe.basic.model.APIResponse;
import com.example.vanthe.basic.ui.base.BasePresenter;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import io.reactivex.Completable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainPresenter<V extends MainContract.View> extends BasePresenter<V> implements MainContract.Presenter<V> {

    private static final String TAG = "MainPresenter";

    public MainPresenter() {
        super();
    }

    public MainPresenter(ApiHelper apiHelper) {
        super(apiHelper);
    }

    @Override
    public void getListData() {
        addObserver(mApiHelper.getDataFromServer()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()),
                this::handleAPIResponse, this::handleThrowable);
    }

    public void handleAPIResponse(JSONArray jsonArray) throws JSONException {
        if (jsonArray != null) {
            getMvpView().loadDataToUI(convertToListString(jsonArray));
        }
        Completable.complete()
                .delay(1, TimeUnit.SECONDS)
                .doOnComplete(() -> getMvpView().hideLoading())
                .subscribe();
    }

    public ArrayList<String> convertToListString(JSONArray jsonArray) throws JSONException {
        getMvpView().showLoading();
        ArrayList<String> listdata = new ArrayList<String>();
        for (int i = 0; i < jsonArray.length(); i++) {
            listdata.add(jsonArray.getString(i));
        }
        return listdata;
    }

    @Override
    public void onViewInitialized() {
        super.onViewInitialized();
    }
}
