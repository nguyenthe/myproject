package com.example.vanthe.basic.ui.base;

import android.support.annotation.NonNull;

import com.androidnetworking.error.ANError;

public interface MVPPresenter<V extends MVPView> {
    void onAttach(V mvpView);

    void onDetach();

    void onViewInitialized();

    void clearDisposableObserver();

    void handleApiError(@NonNull ANError error);

    void handleThrowable(Throwable throwable);

}
