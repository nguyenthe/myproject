package com.example.vanthe.basic.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;

import java.util.Random;

public class UIUtils {

    //function get random color
    public static int getRandomColor() {
        Random rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }


    // function separate one sentence to new line
    public static String getFormatString(String content) {
        String content1 = "";
        String content2 = "";
        String contentParent = "";
        if (content.contains(" ")) {
            String[] parts = content.split(" ");
            for (int i = 0; i < parts.length; i++) {
                if (i < parts.length / 2) {
                    content1 += parts[i] + " ";
                } else {
                    content2 += parts[i] + " ";
                }
            }
            contentParent = content1 + "\n" + content2;
            return contentParent;
        }
        return content;
    }
}
