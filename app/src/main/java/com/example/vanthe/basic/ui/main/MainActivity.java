package com.example.vanthe.basic.ui.main;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.vanthe.basic.R;
import com.example.vanthe.basic.ui.base.BaseActivity;
import com.example.vanthe.basic.ui.main.adapter.HomeAdapter;
import com.example.vanthe.basic.utils.injects.Injections;

import java.util.ArrayList;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends BaseActivity implements MainContract.View, HomeAdapter.ClickListener {

    private MainContract.Presenter mPresenter;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private HomeAdapter homeAdapter;
    private ArrayList<String> mListData = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_layout);
        mPresenter = new MainPresenter(Injections.provideAppDataManger());
        mPresenter.onAttach(this);
        initView();
        initData();
        mPresenter.onViewInitialized();
    }

    @Override
    protected void initView() {
        mRecyclerView = findViewById(R.id.mRecyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        homeAdapter = new HomeAdapter(this, mListData, this);
        mRecyclerView.setAdapter(homeAdapter);
    }

    @Override
    protected void initData() {
        if (hasPermission(READ_EXTERNAL_STORAGE) && hasPermission(WRITE_EXTERNAL_STORAGE)) {
            if (!isNetworkConnected()) {
                //no internet connection
                onNoInternetConnection();
            } else {
                mPresenter.getListData();
            }
        }
    }

    // get data from presenter and show to UI
    @Override
    public void loadDataToUI(ArrayList<String> arrayList) {
        mListData = arrayList;
        homeAdapter.set(mListData);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.getListData();
    }

    @Override
    public void onItemClicked(View v, int position) {
        Toast.makeText(MainActivity.this, "Item position " + position, Toast.LENGTH_LONG).show();
    }
}
