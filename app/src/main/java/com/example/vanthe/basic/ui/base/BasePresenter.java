package com.example.vanthe.basic.ui.base;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.androidnetworking.common.ANConstants;
import com.androidnetworking.error.ANError;
import com.example.vanthe.basic.R;
import com.example.vanthe.basic.data.networking.ApiHelper;

import org.json.JSONArray;

import javax.net.ssl.HttpsURLConnection;

import io.reactivex.functions.Consumer;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class BasePresenter<V extends MVPView> implements MVPPresenter<V> {

    private V mMvpView;
    private CompositeDisposable mCompositeDisposable;
    protected ApiHelper mApiHelper;

    private Consumer<? super JSONArray> mLastOnNext;
    private Consumer<? super Throwable> mLastOnError;
    private Observable<JSONArray> mLastObservable;

    public BasePresenter(ApiHelper apiHelper) {
        this.mApiHelper = apiHelper;
        mCompositeDisposable = new CompositeDisposable();
    }

    public BasePresenter() {
    }

    public V getMvpView() {
        return mMvpView;
    }


    @Override
    public void onAttach(V mvpView) {
        mMvpView = mvpView;
    }

    @Override
    public void onDetach() {
        if (!mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.dispose();
        }
        mMvpView = null;
    }

    public void addDisposableObserver(Disposable disposable) {
        mCompositeDisposable.add(disposable);
    }

    public void addObserver(Observable<JSONArray> observable,
                            Consumer<? super JSONArray> onNext,
                            Consumer<? super Throwable> onError) {

        mLastObservable = observable;
        mLastOnNext = onNext;
        mLastOnError = onError;
        addDisposableObserver(observable.subscribe(onNext, onError));
    }

    @Override
    public void onViewInitialized() {

    }

    @Override
    public void clearDisposableObserver() {
        mCompositeDisposable.clear();
    }

    @Override
    public void handleApiError(@NonNull ANError error) {
        if (error.getErrorBody() == null) {
            getMvpView().onConnectToServerError();
            return;
        }
        if (error.getErrorCode() == HttpsURLConnection.HTTP_UNAUTHORIZED) {
            //function logout
            return;
        }
        getMvpView().onError(TextUtils.isEmpty(error.getMessage()) ? "" : error.getMessage());
    }

    @Override
    public void handleThrowable(Throwable throwable) {
        if (throwable instanceof ANError) {
            handleApiError((ANError) throwable);
        } else {
            getMvpView().onError(R.string.some_error);
        }
    }
}
