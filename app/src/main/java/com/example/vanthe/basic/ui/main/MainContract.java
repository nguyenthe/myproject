package com.example.vanthe.basic.ui.main;

import android.view.View;

import com.example.vanthe.basic.ui.base.MVPPresenter;
import com.example.vanthe.basic.ui.base.MVPView;

import java.util.ArrayList;

public interface MainContract {
    interface Presenter<V extends View> extends MVPPresenter<V>{
        public void getListData();
    }

    interface View extends MVPView{
        void loadDataToUI(ArrayList<String> arrayList);
    }
}
