package com.example.vanthe.basic.data.networking;

import org.json.JSONArray;

import io.reactivex.Observable;

public interface ApiHelper {
    Observable<JSONArray> getDataFromServer();
}
