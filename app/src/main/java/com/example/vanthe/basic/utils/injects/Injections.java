package com.example.vanthe.basic.utils.injects;

import android.content.Context;

import com.example.vanthe.basic.data.networking.ApiHelper;
import com.example.vanthe.basic.data.networking.ApiHelperImpl;

public class Injections {
    public static ApiHelper provideAppDataManger(){
        return ApiHelperImpl.getInstance();
    }
}
