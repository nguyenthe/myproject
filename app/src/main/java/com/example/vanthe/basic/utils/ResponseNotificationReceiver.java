package com.example.vanthe.basic.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.vanthe.basic.ui.dialog.NotifyConnectionDialog;

public class ResponseNotificationReceiver extends BroadcastReceiver {

    public static final String ACTION_RESP = "com.vanthe.intent.action.MESSAGE_PROCESSED";
    public static final String STR_TITLE = "title";
    public static final String STR_BODY = "body";

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            String title = "";
            String body = "";
            if (extras.containsKey(STR_TITLE)) {
                title = extras.getString(STR_TITLE);
            }
            if (extras.containsKey(STR_BODY)) {
                body = extras.getString(STR_BODY);
            }
            NotifyConnectionDialog dialog = new NotifyConnectionDialog(context, title, body);
            dialog.show();
        }
    }
}
