package com.example.vanthe.basic.ui.base;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toolbar;

import com.example.vanthe.basic.R;
import com.example.vanthe.basic.ui.dialog.DialogLoading;
import com.example.vanthe.basic.ui.dialog.NotifyConnectionDialog;
import com.example.vanthe.basic.utils.ResponseNotificationReceiver;
import com.example.vanthe.basic.utils.SystemUtils;

public abstract class BaseActivity extends AppCompatActivity implements MVPView, NotifyConnectionDialog.NotifyConnectionDialogListener {

    private DialogLoading mProgressDialog;
    private NotifyConnectionDialog notifyConnectionDialog;
    private ResponseNotificationReceiver mResponseNotificationReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void registerNotificationReceiver() {
        IntentFilter intentFilter = new IntentFilter(ResponseNotificationReceiver.ACTION_RESP);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        mResponseNotificationReceiver = new ResponseNotificationReceiver();
        registerReceiver(mResponseNotificationReceiver, intentFilter);
    }

    private void unregisterNotificationReceiver() {
        if (mResponseNotificationReceiver != null) {
            unregisterReceiver(mResponseNotificationReceiver);
        }
    }

    @Override
    public void showLoading() {
        if (mProgressDialog == null) {
            mProgressDialog = new DialogLoading(this);
        }
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    @Override
    public void hideLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onErrorDialog(String message) {

    }

    @Override
    public void onError(int resId) {

    }

    @Override
    public void onConnectToServerError() {
        showNoti();
    }

    @Override
    public void onNoInternetConnection() {
        showNoti();
    }

    public void showNoti() {
        if (notifyConnectionDialog == null) {
            notifyConnectionDialog = new NotifyConnectionDialog(BaseActivity.this,
                    "No Internet Connection", "");
            notifyConnectionDialog.setDialogListener(this);
        } else {
            notifyConnectionDialog.setTitle("No Internet Connection");
        }
        if (!notifyConnectionDialog.isShowing()) {
            notifyConnectionDialog.show();

        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void requestPermissionsSafely(String[] permissions, int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean hasPermission(String permission) {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public boolean isNetworkConnected() {
        return SystemUtils.isConnectingToInternet(BaseActivity.this);
    }

    @Override
    public void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }

        }
    }

    @Override
    public void onDismissNotifyConnectionDialog() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    protected abstract void initView();

    protected abstract void initData();

    @Override
    protected void onPause() {
        unregisterNotificationReceiver();
        super.onPause();
    }

    @Override
    protected void onResume() {
        registerNotificationReceiver();
        super.onResume();
    }


}
