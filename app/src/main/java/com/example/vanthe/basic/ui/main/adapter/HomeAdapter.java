package com.example.vanthe.basic.ui.main.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.vanthe.basic.R;
import com.example.vanthe.basic.ui.base.SyncListAdapter;

import java.util.ArrayList;
import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HolderHomeAdapter> implements SyncListAdapter<String> {

    private List<String> mListData;
    private Context mContext;
    private ClickListener mClickListener;

    public HomeAdapter(Context context, List<String> newArray, ClickListener clickListener) {
        this.mContext = context;
        this.mListData = newArray;
        if (this.mListData == null) {
            this.mListData = new ArrayList<>();
        }
        this.mClickListener = clickListener;
    }

    @NonNull
    @Override
    public HolderHomeAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        int layout = R.layout.item_adapter_home_layout;
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(layout, viewGroup, false);
        return new HolderHomeAdapter(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderHomeAdapter holderHomeAdapter, int i) {
        String content = mListData.get(i);
        if (content != null) {
            holderHomeAdapter.bind(content, mClickListener);
        }
    }

    @Override
    public void add(String s) {
        if (!mListData.contains(s)) {
            mListData.add(s);
        }
    }

    @Override
    public void add(List<String> strings) {
        mListData.addAll(strings);
    }

    @Override
    public void set(String s, int pos) {
        mListData.add(pos, s);
    }

    @Override
    public void remove(String s) {
        mListData.remove(s);
    }

    @Override
    public void remove(int index) {
        mListData.remove(index);
    }

    @Override
    public void set(List<String> strings) {
        mListData.clear();
        mListData.addAll(strings);
        notifyDataSetChanged();
    }

    @Override
    public void clear() {
        mListData.clear();
    }

    @Override
    public int getItemCount() {
        return mListData.size();
    }

    @Override
    public String getItem(int position) {
        return mListData.get(position);
    }

    public interface ClickListener {
        void onItemClicked(View v, int position);
    }

    public void setClickListener(ClickListener clickListener) {
        this.mClickListener = clickListener;
    }
}
