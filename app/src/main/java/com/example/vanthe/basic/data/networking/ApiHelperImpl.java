package com.example.vanthe.basic.data.networking;

import com.rx2androidnetworking.BuildConfig;
import com.rx2androidnetworking.Rx2AndroidNetworking;

import org.json.JSONArray;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;

public class ApiHelperImpl implements ApiHelper {

    private static ApiHelperImpl mInstance;
    private static OkHttpClient.Builder okBuilder;
    private static OkHttpClient okHttpClient;

    private static OkHttpClient.Builder okBuilderPrivate;
    private static OkHttpClient okHttpClientPrivate;

    public static synchronized ApiHelperImpl getInstance() {
        if (mInstance == null) {
            mInstance = new ApiHelperImpl();
            okBuilder = new OkHttpClient.Builder();
            okHttpClient = okBuilder.build();
            okBuilderPrivate = new OkHttpClient.Builder();
            okHttpClientPrivate = okBuilderPrivate.build();
        }
        return mInstance;
    }

    public ApiHelperImpl() {
    }

    @Override
    public Observable<JSONArray> getDataFromServer() {
        return Rx2AndroidNetworking.get(ApiEndPoint.BASE_URL)
                .setOkHttpClient(okHttpClient)
                .build()
                .getJSONArrayObservable();
    }
}
