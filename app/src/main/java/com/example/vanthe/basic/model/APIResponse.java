package com.example.vanthe.basic.model;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import java.util.ArrayList;
import java.util.List;

public class APIResponse<T> {
    T t;

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    private JsonElement data = null;

    public List<T> getDataList(Class<T> myClass) {
        JsonArray arrJson = data.getAsJsonArray();
        List<T> listData = new ArrayList<>();
        for (JsonElement jsonE : arrJson) {
            T objectTemp = (new Gson()).fromJson(jsonE, myClass);
            listData.add(objectTemp);
        }
        return listData;
    }
}
