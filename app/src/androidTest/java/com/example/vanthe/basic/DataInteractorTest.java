package com.example.vanthe.basic;

import android.util.Log;

import com.example.vanthe.basic.data.networking.ApiHelper;
import com.example.vanthe.basic.data.networking.ApiHelperImpl;

import org.json.JSONArray;
import org.junit.Before;
import org.junit.Test;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.functions.Consumer;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.TestSubscriber;

import static org.hamcrest.MatcherAssert.assertThat;

public class DataInteractorTest {
    ApiHelper apiHelper;

    @Before
    public void setup(){
        apiHelper = ApiHelperImpl.getInstance();
    }

    @Test
    public void testGetData()throws Exception {
        TestObserver<JSONArray> subscriber = TestObserver.create();
        apiHelper.getDataFromServer().subscribe(subscriber);
        subscriber.assertNoErrors();
        subscriber.assertComplete();
        Log.d(DataInteractorTest.class.getName(), "value " + subscriber.values());

    }
}
