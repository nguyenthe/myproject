package com.example.vanthe.basic;

import com.example.vanthe.basic.data.networking.ApiHelper;
import com.example.vanthe.basic.ui.main.MainContract;
import com.example.vanthe.basic.ui.main.MainPresenter;
import com.example.vanthe.basic.utils.injects.Injections;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import okhttp3.Response;

public class MainPresenterTest {
    @Mock
    MainPresenter mainPresenter;

    @Mock
    MainContract mainContract;

    @Mock
    ApiHelper apiHelper;

    @Mock
    Injections injections;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mainPresenter = Mockito.spy(new MainPresenter(injections.provideAppDataManger()));
    }

    @Test
    public void getData() {
        mainPresenter.getListData();
        Mockito.verify(mainPresenter, Mockito.times(1)).getListData();
    }



}
